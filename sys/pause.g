; pause.g
; called when pause a print
;

G1 E-5 F900 ; retract 5mm
G1 Z100     ; move head up 100mm