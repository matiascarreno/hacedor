; homez.g
; called to home the Z axis
;

; increase Z
G91 ; relative positioning
G1 Z10 ; move Z relative to current position to avoid dragging nozzle over the bed
G90 ; absolute positioning

; home Z
var xCenter = move.compensation.probeGrid.mins[0] + (move.compensation.probeGrid.maxs[0] - move.compensation.probeGrid.mins[0]) / 2 - sensors.probes[0].offsets[0]
var yCenter = move.compensation.probeGrid.mins[1] + (move.compensation.probeGrid.maxs[1] - move.compensation.probeGrid.mins[1]) / 2 - sensors.probes[0].offsets[1]
G1 X{var.xCenter} Y{var.yCenter} F6000 ; go to bed centre
G30 ; probe the bed
