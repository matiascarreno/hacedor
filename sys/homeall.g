; homeall.g
; called to home all axes
;

; increase Z
G91                                                   ; relative positioning
G1 H2 Z10 F6000                                       ; move Z relative to current position to avoid dragging nozzle over the bed
G90                                                   ; absolute positioning

; home XY
var xTravel = move.axes[0].max - move.axes[0].min + 5 ; calculate how far X can travel plus 5mm
var yTravel = move.axes[1].max - move.axes[1].min + 5 ; calculate how far Y can travel plus 5mm
G91                                                   ; relative positioning
G1 H1 X{-var.xTravel} Y{var.yTravel} F9000            ; coarse home
G1 H1 X{-var.xTravel} F9000                           ; coarse home in the -X direction
G1 H1 Y{var.yTravel} F9000                            ; coarse home in the +Y direction
G1 X5 Y-5 F9000                                       ; move back 5mm
G1 H1 X{-var.xTravel} Y{var.yTravel} F300             ; fine home
G1 H1 X{-var.xTravel} F300                            ; fine home in the -X direction
G1 H1 Y{var.yTravel} F300                             ; fine home in the +Y direction
G90                                                   ; absolute positioning

; home Z
var xCenter = move.compensation.probeGrid.mins[0] + (move.compensation.probeGrid.maxs[0] - move.compensation.probeGrid.mins[0]) / 2 - sensors.probes[0].offsets[0]
var yCenter = move.compensation.probeGrid.mins[1] + (move.compensation.probeGrid.maxs[1] - move.compensation.probeGrid.mins[1]) / 2 - sensors.probes[0].offsets[1]
G1 X{var.xCenter} Y{var.yCenter} F9000                ; go to bed centre
G30                                                   ; probe the bed