; config-override.g file generated in response to M500 at 2024-07-25 21:37
; This is a system-generated file - do not edit
; Heater model parameters
M307 H0 R0.622 K0.445:0.000 D3.73 E1.35 S1.00 B0
M307 H1 R2.071 K0.942:0.000 D1.92 E1.35 S1.00 B0 V24.0
; Workplace coordinates
G10 L2 P1 X0.00 Y0.00 Z0.00
G10 L2 P2 X0.00 Y0.00 Z0.00
G10 L2 P3 X0.00 Y0.00 Z0.00
G10 L2 P4 X0.00 Y0.00 Z0.00
G10 L2 P5 X0.00 Y0.00 Z0.00
G10 L2 P6 X0.00 Y0.00 Z0.00
G10 L2 P7 X0.00 Y0.00 Z0.00
G10 L2 P8 X0.00 Y0.00 Z0.00
G10 L2 P9 X0.00 Y0.00 Z0.00
