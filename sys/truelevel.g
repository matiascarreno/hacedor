; bed.g
; called to level the bed
;

;M140 S60                  ; heat bed to measure deviations

G28                       ; Home all axis

M558 H5                   ; Lift head by 10mm when probing
G1 X5 Y5 F12000           ; move to point
G30 P0 X5 Y5 Z-99999      ; probe near a leadscrew
G1 X5 Y295 F12000         ; move to point
G30 P1 X5 Y295 Z-99999    ; probe near a leadscrew
G1 X295 Y295 F12000       ; move to point
G30 P2 X295 Y295 Z-99999  ; probe near a leadscrew
G1 X295 Y5 F12000         ; move to point
G30 P3 X295 Y5 Z-99999 S4 ; probe near a leadscrew and calibrate 4 motors

M558 H2                   ; Lift head by 5mm when probing

G1 X5 Y5 F12000           ; move to point
G30 P0 X5 Y5 Z-99999      ; probe near a leadscrew
G1 X5 Y295 F12000         ; move to point
G30 P1 X5 Y295 Z-99999    ; probe near a leadscrew
G1 X295 Y295 F12000       ; move to point
G30 P2 X295 Y295 Z-99999  ; probe near a leadscrew
G1 X295 Y5 F12000         ; move to point
G30 P3 X295 Y5 Z-99999 S4 ; probe near a leadscrew and calibrate 4 motors